import urllib, json

wikiroot = "http://wiki.thacker.com.br/api.php?"
padroot = "http://pad.thacker.com.br:9001/"
padapi = "api/1/getText?apikey=TQYdwqyFXAZFLwPKWFwGuQHLTVvkPzG6&padID="

def padtowiki(pagina):
    padurl = padroot + padapi + pagina
    tokenurl = wikiroot + 'action=query&prop=info|revisions&intoken=edit&titles=Main%20Page&format=json'
    tokenpage = urllib.urlopen(tokenurl).read()
    tokenpage = json.loads(tokenpage)
    for c in tokenpage['query']['pages']:
        token = tokenpage['query']['pages'][c]['edittoken']
    pad = urllib.urlopen(padurl)
    padtext = json.load(pad)['data']['text'] + '\n\n\n[[Category:Pad]]'
    wikiapi_params = {
        'action' : 'edit',
        'title' : pagina,
        'summary' : 'Updating from pad',
        'text' : padtext.encode('utf-8'),
        'token' : token.encode('utf-8')
        }

    vixe = urllib.urlopen(wikiroot, urllib.urlencode(wikiapi_params)).read()

    return vixe 

def index(req):
    GET = dict([ i.split('=') for i in req.subprocess_env[ 'QUERY_STRING'].split('&')])
    result = padtowiki(GET['pad'])
    print "Content-Type: text/html \n\n"
    return result
